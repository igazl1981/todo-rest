package com.training.reactive.todorest.web.application

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class TodoRestApplication

fun main(args: Array<String>) {
    runApplication<TodoRestApplication>(*args)
}
