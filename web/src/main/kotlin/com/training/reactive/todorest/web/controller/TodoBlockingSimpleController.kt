package com.training.reactive.todorest.web.controller

import com.training.reactive.todorest.TodoService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * The controller uses the Blocking way to return the Todos from a simple repository.
 */
@RestController
class TodoBlockingSimpleController(private val todoService: TodoService) {

    @GetMapping("/v1/todo")
    fun getAll(): String {
        return "List of Todos"
    }

}