# Todo Rest - Reactive learning project

This project is used to learn reactive flow with Kotlin and Spring Boot.

## Plan

1. Create the Todo Rest endpoints with the traditional way
   1. Use static storage (eg. Map<LocalDateTime, Todo>)
   2. Use database usage (eg. MongoDb)

2. Create Reactive endpoints
   1. Use static storage
   2. Use database
    
## API

TBD